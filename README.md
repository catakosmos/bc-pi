# BC-pi

># INTRODUCCIÓN
##### Día a día podemos ver como la implementación de las bicicletas se va abriendo paso entre las calles, dadas sus características eco amigables se han vuelto un atractivo notable para la juventud consciente de la contaminación que generan los automóviles. Según la Encuesta Nacional de Medio Ambiente, cuyos resultados fueron presentados en febrero de 2018, el 8% de la población que reside en la Región Metropolitana utiliza la bicicleta como principal medio de transporte. Sin embargo, hay que tener consideración con los accidentes que estas pueden provocar; según la CONASET el 5% de los fallecidos en el tránsito son ciclistas y solo durante el año 2019, la bicicleta participó en 3.840 siniestros viales y resultaron 85 ciclistas fallecidos y 3.199 lesionados.
##### Es por esta razón que decidimos crear BC-pi, un aparato que por medio de sensores de ultrasonido mide la distancia de la bicicleta con respecto a medios de transporte cercanos que se encuentren dentro de una distancia mínima lateral de 1,5 metros, ya sea lateralmente o tras el/la conductor/a de la bicicleta.

##### Datos obtenidos de:
[](url)https://www.pauta.cl/nacional/cada-ano-hay-mas-ciclistas-en-santiago-y-cada-ano-mueren-menos-de-ellos

[](url)https://www.conaset.cl/ciclistas/



